# Script to warn if users have to much lists in Sympa

Warning: only works with PostgreSQL

## Installation

```
cpan Carton
cd /opt/
git clone https://framagit.org/framasoft/supervision-sympa
cd supervision-sympa
carton install --deployment
```

## How to use

Put this in your `postgres` user’s crontab:
```
* * * * * cd /opt/supervision-sympa && /usr/local/bin/carton exec ./watch_list_nb.pl
```

If warned about a user but you think it’s OK for him/her to have so much lists, add its email address in `WatchListGoodCollection.pm`;

If you want to delete all the lists of a user and ban him/her, you can use this script: <https://framagit.org/-/snippets/6157>.

## LICENSE

This script is licensed under the terms of the WTFPL. See the [LICENSE](LICENSE) file.
