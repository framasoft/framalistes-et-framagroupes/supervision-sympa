package WatchListGoodCollection;
use Mojo::Base -base;
use Mojo::Collection 'c';
use Exporter;

our @ISA    = qw(Exporter);
our @EXPORT = qw($good_collection);

our $good_collection = c(
    'foo@bar.org', 'baz@glux.org',
);

1;
